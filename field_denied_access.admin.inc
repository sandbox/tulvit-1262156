<?php

/**
 * @file
 * Administration page callbacks for the field denied access module.
 */

/**
 * Form builder. Configure which fields will be denied.
 */
function field_denied_access_admin_settings() {
  $fields = field_info_fields();
  foreach ($fields as $key => $value) {
    if (strpos($key, 'field') !== FALSE) {
      $options[$key] = $key;
    }
  }
  $form['field_denied_access'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Define which fields must be access denied.'),
    '#options' => $options,
    '#default_value' => variable_get('field_denied_access', array()),
    '#description' => t('Denied fields\' access so no one can\'t change their values by using user interface.'),
  );
  return system_settings_form($form);
}
